import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth.service';
import {User} from "../../models/User";
import {UsersService} from "../../users/users.service";

@Component({
  selector: 'app-side-nav',
  templateUrl: './app-side-nav.component.html',
  styleUrls: ['./app-side-nav.component.scss']
})
export class AppSideNavComponent implements OnInit {

  user: User;


  constructor(public auth: AuthService, public userService: UsersService) {
    this.auth.user.subscribe((user) => {
      this.user = user;

      console.log(user);
    });
  }

  ngOnInit() {
  }
 // routeNames = ["dashboard","profile"];
}
