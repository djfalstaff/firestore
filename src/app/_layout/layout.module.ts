import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {AppBreadcrumbComponent} from './app-breadcrumb/app-breadcrumb.component';
import {AppHeaderComponent} from './app-header/app-header.component';
import {AppSideNavComponent} from './app-side-nav/app-side-nav.component';
import {NotificationMessageComponent} from './notification-message/notification-message.component';
import {LoadingSpinnerComponent} from './loading-spinner/loading-spinner.component';
import {AppLayoutComponent} from './app-layout/app-layout.component';
import { MaterializeModule } from 'angular2-materialize';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterializeModule
  ],
  declarations: [
    AppBreadcrumbComponent,
    AppHeaderComponent,
    AppSideNavComponent,
    NotificationMessageComponent,
    LoadingSpinnerComponent,
    AppLayoutComponent
  ]
})
export class LayoutModule { }
