import { AppLayoutComponent } from './_layout/app-layout/app-layout.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { MaterializeModule } from 'angular2-materialize';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ForgotPasswordComponent } from './forgotpassword/forgotpassword.component';

// Core
import { CoreModule } from './core/core.module';

import { LayoutModule } from './_layout/layout.module'

import {UsersModule} from './users/users.module';

import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';

export const firebaseConfig = environment.firebaseConfig;
import { AngularFirestoreModule } from 'angularfire2/firestore';
import {AdminGuard} from "./core/admin.guard";

@NgModule({
  declarations: [
    AppComponent,
   LoginComponent,
   DashboardComponent,
   RegisterComponent,
   ProfileComponent,
   NotFoundComponent,
   ForgotPasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterializeModule,
    CoreModule,
    LayoutModule,
    UsersModule,
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [AdminGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
