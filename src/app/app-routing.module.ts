import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth.guard';
import { AdminGuard } from './core/admin.guard';
import { AppLayoutComponent } from './_layout/app-layout/app-layout.component';

import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {UsersComponent} from './users/users.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import {ForgotPasswordComponent} from './forgotpassword/forgotpassword.component';
import {NotFoundComponent} from './not-found/not-found.component';
import { CoreModule } from './core/core.module';

const routes: Routes =[
  // App routes goes here here
  {
    path: '',
    component: AppLayoutComponent,
    children: [
      {	path: '', redirectTo: 'dashboard',pathMatch: 'full'},
      { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
      { path: 'users', component: UsersComponent, canActivate: [AdminGuard] },
      { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]  },
      { path: 'campaigns', component: ProfileComponent, canActivate: [AuthGuard]  },
      { path: 'characters', component: ProfileComponent, canActivate: [AuthGuard]  },
      { path: 'npcs', component: ProfileComponent, canActivate: [AuthGuard]  },
      { path: 'monsters', component: ProfileComponent, canActivate: [AuthGuard]  }
    ]
},

//no layout routes
{ path: 'login', component: LoginComponent},
{ path: 'register', component: RegisterComponent },
{ path: 'forgot-password', component: ForgotPasswordComponent },
// last resort 404 - Must be last route always
{ path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard,AdminGuard]
})
export class AppRoutingModule { }
