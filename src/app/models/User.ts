export interface Roles {
  player?: boolean;
  gm?: boolean;
  admin?:  boolean;
  disabled?: boolean;
}

export class User {
    uid: string;
    email?: string;
    photoURL?: string;
    displayName?: string;
    displayRole?: string;
    roles?: Roles;

    constructor(authData) {
      this.email    = authData.email
      this.photoURL = authData.photoURL
      this.displayName = authData.displayName
      this.displayRole = "Player"
      this.roles    = { player: true,  disabled:false, admin:false }
    }
  }
