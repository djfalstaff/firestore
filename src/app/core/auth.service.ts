import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { NotifyService } from './notify.service';
import {User} from '../models/User';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';

@Injectable()
export class AuthService {

 // user: Observable<User>;
  user: BehaviorSubject<User> = new BehaviorSubject(null)

  constructor(private afAuth: AngularFireAuth,
              private afs: AngularFirestore,
              private router: Router,
              private notify: NotifyService) {

                this.afAuth.authState
                .switchMap(auth => {
                  if (auth) {
                    /// signed in
                   // return this.db.object('users/' + auth.uid)
                   return this.afs.doc<User>(`users/${auth.uid}`).valueChanges()
                  } else {
                    /// not signed in
                    return Observable.of(null)
                  }
                })
                .subscribe(user => {
                  this.user.next(user)
                })

      // this.user = this.afAuth.authState
      //   .switchMap(user => {
      //     if (user) {
      //       return this.afs.doc<User>(`users/${user.uid}`).valueChanges()
      //     } else {
      //       return Observable.of(null)
      //     }
      //   })

  }

  ////// OAuth Methods /////


  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider()
    return this.oAuthLogin(provider);
  }

  githubLogin() {
    const provider = new firebase.auth.GithubAuthProvider()
    return this.oAuthLogin(provider);
  }

  facebookLogin() {
    const provider = new firebase.auth.FacebookAuthProvider()
    return this.oAuthLogin(provider);
  }

  twitterLogin() {
    const provider = new firebase.auth.TwitterAuthProvider()
    return this.oAuthLogin(provider);
  }


  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        this.notify.update('Welcome to Firestarter!!!', 'success')
        return this.updateUserData(credential.user)
      })
      .catch(error => this.handleError(error) );
  }


  //// Anonymous Auth ////

  anonymousLogin() {
    return this.afAuth.auth.signInAnonymously()
      .then((user) => {
        this.notify.update('Welcome to Firestarter!!!', 'success')
        return this.updateUserData(user) // if using firestore
      })
      .catch(error => this.handleError(error) );
  }

  //// Email/Password Auth ////

  emailSignUp(email: string, password: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((user) => {
        this.notify.update('Welcome to Firestarter!!!', 'success')
        return this.updateUserData(user) // if using firestore
      })
      .catch(error => this.handleError(error) );
  }

  emailLogin(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((user) => {
        this.notify.update('Welcome to Firestarter!!!', 'success')
        return this.updateUserData(user) // if using firestore
      })
      .catch(error => this.handleError(error) );
  }

  // Sends email allowing user to reset password
  resetPassword(email: string) {
    const fbAuth = firebase.auth();

    return fbAuth.sendPasswordResetEmail(email)
      .then(() => this.notify.update('Password update email sent', 'info'))
      .catch((error) => this.handleError(error) )
  }


  signOut() {
    this.afAuth.auth.signOut().then(() => {
     // debugger;
        this.router.navigate(['/login']);
    })
  }

  // If error, console log and notify user
  private handleError(error) {
    console.error(error)
    this.notify.update(error.message, 'error')
  }

  // Sets user data to firestore after succesful login
  private updateUserData(authData) {
    const userData = new User(authData);
    const roles = userData.roles;
    userData.roles = {};
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${authData.uid}`);
    const dbUser = this.afs.doc(`users/${authData.uid}`).ref.get().then( userSnapShot => {
      console.log(userSnapShot);
      if (userSnapShot.exists) {
        console.log(userSnapShot.data());
        if (!userSnapShot.data().roles) {
          userData.roles = roles;
          userRef.set({ ...userData });
        }
      }
    }).catch( err => {
      console.log(err);
    });
    console.log(userRef,'test1');

     return userRef.set({ ...userData }, {merge: true});
  }

}
