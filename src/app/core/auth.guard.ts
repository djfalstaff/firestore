import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService} from './auth.service';
import { NotifyService } from './notify.service';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import * as _ from 'lodash'

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router, private notify: NotifyService) {}


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {

      // return this.auth.user
      // .take(1)
      // .map(user => {
      //  console.log(user);
      //  return _.has(_.get(user, 'roles'), 'player');
      // })
      // .do(authorized => {
      //   if (!authorized) {
      //     console.log('route prevented!')
      //    //  this.router.navigate(['/']);
      //   }
      // })

      return this.auth.user
           .take(1)
           .map(user => {
             //const hasRole = user ? user.roles.player : false;
             return user!=null
            })
           .do(loggedIn => {
             if (!loggedIn) {
               console.log('access denied')
               this.notify.update('You must be logged in!', 'error')
               this.router.navigate(['/login']);
             }
         })

  }
}
