import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService} from './auth.service';
import { NotifyService } from './notify.service';
import * as _ from 'lodash';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router, private notify: NotifyService) {}


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | boolean {
    // let valid: boolean;
    //   this.auth.user
    //   .subscribe(loggedInUser => {
    //     if (!loggedInUser.roles.admin) {
    //       console.log('access denied')
    //       this.notify.update('You must be an Admin to access page', 'error')
    //       this.router.navigate(['/']);
    //       valid = false;
    //     }
    //     valid =  true;
    // })
    // return valid;

    return this.auth.user
    .take(1)
    .map(user => {
      const hasRole = user ? user.roles.admin : false;
      return hasRole;
     })
    .do(loggedIn => {
      if (!loggedIn) {
        console.log('admin access denied')
        this.notify.update('You must be logged in as admin', 'error')
        this.router.navigate(['/dashboard']);
      }
  })

  }
}
