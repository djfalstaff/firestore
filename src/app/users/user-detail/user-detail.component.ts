import { Component, OnInit,Input  } from '@angular/core';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  @Input()
  user: any;
  constructor(private usersService: UsersService) { }

  ngOnInit() {
  }

}
