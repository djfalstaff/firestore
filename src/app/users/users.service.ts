import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import {User} from '../models/User'
import 'rxjs/add/operator/map';
import { AuthService } from '../core/auth.service';
import * as _ from 'lodash'

@Injectable()
export class UsersService {
  usersCollection: AngularFirestoreCollection<User>;
  userDocument:   AngularFirestoreDocument<User>;

  userRoles: Array<string> = []; // roles of currently logged in uer

  constructor(private afs: AngularFirestore,
    private auth: AuthService) {

      auth.user.map(user => {
        /// Set an array of user roles, ie ['admin', 'author', ...]
        const roles = _.get(user, 'roles');
        for (const key in roles) {
          if (roles[key]) {
             this.userRoles.push(key);
          }
        }
        return this.userRoles;
      })
      .subscribe()

    this.usersCollection = this.afs.collection('users', ref =>  ref.orderBy('displayName', 'asc').limit(25));
   }

     ///// Authorization Logic /////
  get canRead(): boolean {
    const allowed = ['admin']
    return this.matchingRole(allowed)
  }
  get canEdit(): boolean {
    const allowed = ['admin', 'gm']
    return this.matchingRole(allowed)
  }

    /// Helper to determine if any matching roles exist
    private matchingRole(allowedRoles): boolean {
      return !_.isEmpty(_.intersection(allowedRoles, this.userRoles))
    }

   getData(): Observable<User[]> {
    if ( this.canRead ) {
      return this.usersCollection.valueChanges();
    }
  }

  getSnapshot() {
    // ['added', 'modified', 'removed']
    if (this.canRead) {
      return this.usersCollection.snapshotChanges().map(actions => {
        return actions.map(a => {
          return { id: a.payload.doc.id, ...a.payload.doc.data() }
        });
      });
    }

  }
  getUser(id) {
    return this.afs.doc<User>('notes/' + id);
  }

  // create(content: string) {
  //   const note: Note = {
  //     content: content,
  //     hearts: 0,
  //     time: new Date().getTime()
  //   }
  //   return this.usersCollection.add(note);
  // }

  updateUser(id, data) {
    return this.getUser(id).update(data)
  }

  // deleteNote(id) {
  //   return this.getNote(id).delete()
  // }

}
