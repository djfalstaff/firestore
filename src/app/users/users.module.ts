import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UsersService } from './users.service';
import {UsersComponent} from './users.component';
import { UserDetailComponent } from './user-detail/user-detail.component';

import { AngularFirestoreModule } from 'angularfire2/firestore';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AngularFirestoreModule.enablePersistence(),
  ],
  declarations: [
    UsersComponent,
    UserDetailComponent
  ],
  providers: [UsersService]
})
export class UsersModule { }
