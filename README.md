
# Angular + Firebase Web App

A basic Angular 5 app powered by Firebase. It can serve as a foundation to learn this stack and roll out more complex features.


## Features

- Angular 5.0
- Firebase Auth with Custom Data in Firestore
- User Management


## Usage

- Create an account at https://firebase.google.com/
- Turn on Google Auth
- add this code snippet to your database rules

#### Database Rules
```typescript
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read, write: if true;
    }


  }
}
```

and get the api info from your account to use in the enviroment.ts files (see below)

- clone this repo
- `cd firestore`
- `npm install`

Create the environment files below in `src/environments/`.

#### environment.ts
```typescript
export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: 'APIKEY',
        authDomain: 'DEV-APP.firebaseapp.com',
        databaseURL: 'https://DEV-APP.firebaseio.com',
        projectId: 'DEV-APP',
        storageBucket: 'DEV-APP.appspot.com',
        messagingSenderId: '123456789'
    }
};
```
#### environment.prod.ts
```typescript
export const environment = {
    production: true,
    firebaseConfig: {
        // same as above, or use a different firebase project to isolate environments
    }
};
```

And finally `ng serve`
